Feature: Testing localhost:8080/actuator/health endpoint

  Background:
    * url 'http://localhost:8080'

  Scenario: Retrieve health status
    Given path '/actuator/health'
    When method GET
    Then status 200
    And match response == { "status": "UP" }
    # Add more assertions as needed
  Scenario: Simulate concurrent clients
    * def concurrentClients = 5  # You can adjust the number of concurrent clients as needed
    
    * def results = karate.repeat(concurrentClients, 'counter') {
        * call read('src/test.feature')  # Include the path to the feature with the above scenario
      }

    * print 'Results:', results
