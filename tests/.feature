Feature: Sample API Test

  Background:
    * url 'http://localhost:8080'

  Scenario: Make a GET request to the sample API endpoint
    Given path '/actuator/health'
    When method GET
    Then status 200
    And match response == { "status": "UP" }
